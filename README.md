ILikeCheckers
==========

Esta es una implementación en python de un juego de Damas utilizando el algoritmo Minimax para las decisiones tomadas por la computadora mediante funciones de utilidad.

Dependencias
-----------
    python 2.7.x (no 3.x)


Uso
-----
    Para comenzar una partida, mediante un terminal entre al directorio donde está el archivo 'main.py' y ejecute:

    `python main.py`

    El programa le preguntará que tipo de juego quiere iniciar (Humano vs IA, IA vs IA o Humano vs Humano)

Modo de juego
-----
    Los jugadores humanos deben ingersar la letra correspondiente a la fila con la columna y la nueva posición de igual manera separado por un espacio.

    Ejemplo, si deseamos movernos de g5 a f6, debemos ingresar "g5 f6".

Utilidad de IA
-----
    La función de utilidad de implementada con identificador 0 hace que los estados que mantengan las piezas más cerca y también donde haya una oportunidad de comer una pieza enemiga sean más provechosos (más utiles).

    Si no se escoge la funcion de utilidad 0, el número ingresado será el puntaje que obtendra cada estado, siendo una elección cualquiera.