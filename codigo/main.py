# coding: utf-8
from board import *
import minmax
import os, time
from copy import deepcopy

def juegaHumanoBlancas(b):
    print "Introduzca la posicion de la dama que quiere mover seguido de la nueva posicion"
    while True:
        move = []
        move = raw_input().lower().split()
        if not(len(move) == 2):
            print "Este no es un movimiento posible"
            print statement1
            continue
        desde = (int(move[0][1]), ord(move[0][0]) - 97)
        hasta = (int(move[1][1]), ord(move[1][0]) - 97)
        if not (desde in b.whitelist):
            print "Dama invalida: ", desde, ". Escoja un movimiento válido"
            continue
        break
    move = (desde, hasta, b.NOTDONE)
    return move

def juegaHumanoNegras(b):
    print "Introduzca la posicion de la dama que quiere mover seguido de la nueva posicion"
    while True:
        move = []
        move = raw_input().lower().split()
        if not(len(move) == 2):
            print "Este no es un movimiento posible"
            print statement1
            continue
        desde = (int(move[0][1]), ord(move[0][0]) - 97)
        hasta = (int(move[1][1]), ord(move[1][0]) - 97)
        if not (desde in b.blacklist):
            print "Dama invalida: ", desde, ". Escoja un movimiento válido"
            continue
        break
    move = (desde, hasta, b.NOTDONE)
    return move

if __name__ == '__main__':
    b = board(8, 8, 0)
    print "Bienvenido a ILikeCheckers."
    print "Introduzca 0 para IA vs IA, 1 para Humano vs IA y 2 para Humano vs Humano"
    gametype = int(raw_input())
    if gametype == 1:
       print "Que funcion de utilidad deberiamos usar para IA?"
       print "0: Piezas mas juntas, buscando la posibilidad de comer una pieza del oponente"
       print "Cualquier otro numero: se asume que la utilidad de cualquier movimiento es el numero ingresado"
       utilityIA1 = int(raw_input())
    elif gametype == 0:
       print "Que funcion de utilidad deberiamos usar para IA 1?"
       print "0: Piezas mas juntas, buscando la posibilidad de comer una pieza del oponente"
       print "Cualquier otro numero: se asume que la utilidad de cualquier movimiento es el numero ingresado"
       utilityIA1 = int(raw_input())       
       print "Que funcion de utilidad deberiamos usar para IA? 2"
       print "0: Piezas mas juntas, buscando la posibilidad de comer una pieza del oponente"
       print "Cualquier otro numero: se asume que la utilidad de cualquier movimiento es el numero ingresado"
       utilityIA2 = int(raw_input())       
    moves = 1
    b.printBoard()
    while True:
        print "Move... ",moves
        if gametype == 1 or gametype == 2:
            print "Turno humano (player 1)!", u'◇'
            b.printBoard()
            userMove = juegaHumanoBlancas(b)
            while True:
                try:
                   b.moveWhite(*userMove)
                except Exception:
                   print "Movimiento invalido, intente nuevamente"
                   userMove = juegaHumanoBlancas(b)
                   continue
                break
        if gametype == 0:
            print "IA 1 (blancas) esta pensando...", u'◇'
            temp = minmax.minMax2(b,utilityIA1)
            b = temp[0]
            print "**********IA********** (Player 1) ", u'◇'
            b.printBoard()
            if b.checkWon() == b.WHITE:
                print "Blancas ganan"
                break
            elif b.checkWon() == b.BLACK:
                print "Negras ganan"
                break
        if gametype == 0 or gametype == 1:
            print "IA 2 (negras) esta pensando...", u'◆'
            if gametype == 0:
               temp = minmax.minMax2(b,utilityIA2)
            else: temp = minmax.minMax2(b,utilityIA1)
            b = temp[0]
            print "**********IA********** (Player 2)", u'◆'
            b.printBoard()
            if b.checkWon() == b.WHITE:
                print "Blancas ganan"
                break
            elif b.checkWon() == b.BLACK:
                print "Negras ganan"
                break
            moves += 1
        if gametype == 2:
            print "Turno Humano (player 2)!", u'◆'
            b.printBoard()
            userMove = juegaHumanoNegras(b)
            while True:
                try:
                   b.moveBlack(*userMove)
                except Exception:
                   print "Movimiento invalido, intente nuevamente"
                   userMove = juegaHumanoNegras(b)
                   continue
