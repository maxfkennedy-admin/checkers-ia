from copy import deepcopy
from utility import *

def is_won(board):
    return board.gameWon <> board.NOTDONE
        
def minMax2(board, utilityValue):
    bestBoard = None
    currentDepth = board.maxDepth + 1
    while not bestBoard and currentDepth > 0:
        currentDepth -= 1
        (bestBoard, bestVal) = maxMove2(board, currentDepth, utilityValue)
    if not bestBoard:
        raise Exception("No hay movimientos! no deberia ocurrir")
    else:
        return (bestBoard, bestVal)

def maxMove2(maxBoard, currentDepth, utilityValue):
    return maxMinBoard(maxBoard, currentDepth-1, utilityValue, float('-inf'))

def minMove2(minBoard, currentDepth, utilityValue):
    return maxMinBoard(minBoard, currentDepth-1, utilityValue, float('inf'))

def maxMinBoard(board, currentDepth, utilityValue, bestMove):
    if is_won(board) or currentDepth <= 0:
        return (board, utility(board, utilityValue))
    best_move = bestMove
    best_board = None    
    if bestMove == float('-inf'):
        if board.turn == board.WHITE:
           moves = board.iterWhiteMoves()
        else:
           moves = board.iterBlackMoves()
        for move in moves:
            maxBoard = deepcopy(board)
            if board.turn == board.WHITE:
               maxBoard.moveSilentWhite(*move)
            else:
               maxBoard.moveSilentBlack(*move)
            value = minMove2(maxBoard, currentDepth-1, utilityValue)[1]
            if value > best_move:
                best_move = value
                best_board = maxBoard         
    elif bestMove == float('inf'):
        if board.turn == board.WHITE:
           moves = board.iterBlackMoves()
        else:
           moves = board.iterWhiteMoves()
        for move in moves:
            minBoard = deepcopy(board)
            if board.turn == board.WHITE:
               minBoard.moveSilentBlack(*move)
            else:
               minBoard.moveSilentWhite(*move)
            value = maxMove2(minBoard, currentDepth-1, utilityValue)[1]
            if value < best_move:
                best_move = value
                best_board = minBoard
    else:
        raise Exception("No hay movimientos!")
    return (best_board, best_move)
