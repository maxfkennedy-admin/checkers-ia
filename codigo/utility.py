def utility(evalBoard,kind):
    score = 0
    pieces = None 
    scoremod = 1

    if evalBoard.turn == evalBoard.WHITE:
        pieces = evalBoard.whitelist
    elif evalBoard.turn == evalBoard.BLACK:
        pieces = evalBoard.blacklist

    if kind != 0:
        return kind

    distance = 0
    for piece1 in pieces:
        for piece2 in pieces:
            if piece1 == piece2:
                continue
            dx = abs(piece1[0] - piece2[0])
            dy = abs(piece1[1] - piece2[1])
            distance += dx**2 + dy**2
    distance /= len(pieces)
    if distance * scoremod == 0: return 0
    score = 1.0/distance * scoremod
    return score
