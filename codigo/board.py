# coding: utf-8
class board(object):
    BLACK = 1
    WHITE = 0
    NOTDONE = -1
    def __init__(self, height, width, firstPlayer):
        self.width = width
        self.height = height
        self.blacklist = []
        self.whitelist = []
        for i in range(width):
            self.blacklist.append((i, (i+1)%2))
            self.whitelist.append((i, height - (i%2) - 1))
        self.boardState = [[' '] * self.width for x in range(self.height)]
        self.gameWon = self.NOTDONE
        self.turn = firstPlayer
        self.maxDepth = 10
    
    def iterWhiteMoves(self):
        for piece in self.whitelist:
            for move in self.iterWhitePiece(piece):
                yield move
                
    def iterBlackMoves(self):
        for piece in self.blacklist:
            for move in self.iterBlackPiece(piece):
                yield move
                
    def iterWhitePiece(self, piece):
        return self.iterBoth(piece, ((-1,-1),(1,-1)))
    
    def iterBlackPiece(self, piece):
        return self.iterBoth(piece, ((-1,1),(1,1)))

    def iterBoth(self, piece, moves):
        for move in moves:
            targetx = piece[0] + move[0]
            targety = piece[1] + move[1]
            if targetx < 0 or targetx >= self.width or targety < 0 or targety >= self.height:
                continue
            target = (targetx, targety)
            black = target in self.blacklist
            white = target in self.whitelist
            if not black and not white:
                yield (piece, target, self.NOTDONE)
            else:
                if self.turn == self.BLACK and black:
                    continue
                elif self.turn == self.WHITE and white:
                    continue
                jumpx = target[0] + move[0]
                jumpy = target[1] + move[1]
                if jumpx < 0 or jumpx >= self.width or jumpy < 0 or jumpy >= self.height:
                    continue
                jump = (jumpx, jumpy)
                black = jump in self.blacklist
                white = jump in self.whitelist
                if not black and not white:
                    yield (piece, jump, self.turn)                   
    
    def updateBoard(self):
        for i in range(self.width):
            for j in range(self.height):
                self.boardState[i][j] = " "
        for piece in self.blacklist:
            self.boardState[piece[1]][piece[0]] = u'◆'
        for piece in self.whitelist:
            self.boardState[piece[1]][piece[0]] = u'◇'

    def checkWon(self):
        for piece in self.blacklist:
            if piece[1] == self.height-1:
                return self.BLACK
        for piece in self.whitelist:
            if piece[1] == 0:
                return self.WHITE        

    def moveSilentBlack(self, moveFrom, moveTo, winLoss): 
        if moveTo[0] < 0 or moveTo[0] >= self.width or moveTo[1] < 0 or moveTo[1] >= self.height:
            raise Exception("Debemos mantenernos dentro de los margenes")
        black = moveTo in self.blacklist
        white = moveTo in self.whitelist
        if not (black or white):
            self.blacklist[self.blacklist.index(moveFrom)] = moveTo
            self.updateBoard()
            self.turn = self.WHITE
            if moveTo[0]-moveFrom[0] > 1: #derecha
                if (moveTo[0]-1,moveTo[1]-1) in self.whitelist:
                   self.whitelist.pop(self.whitelist.index((moveTo[0]-1,moveTo[1]-1)))
            elif moveTo[0]-moveFrom[0] < -1: #izquierda
                if (moveTo[0]+1,moveTo[1]-1) in self.whitelist:
                   self.whitelist.pop(self.whitelist.index((moveTo[0]+1,moveTo[1]-1)))
            self.gameWon = -1
        else:
            raise Exception
        
    def moveSilentWhite(self, moveFrom, moveTo, winLoss):
        if moveTo[0] < 0 or moveTo[0] >= self.width or moveTo[1] < 0 or moveTo[1] >= self.height:
            raise Exception("Debemos mantenernos dentro de los margenes")
        black = moveTo in self.blacklist
        white = moveTo in self.whitelist
        if not (black or white):
            self.whitelist[self.whitelist.index(moveFrom)] = moveTo
            self.updateBoard()
            self.turn = self.BLACK
            if moveFrom[0]-moveTo[0] > 1: #izquierda
                if (moveTo[0]+1,moveTo[1]+1) in self.blacklist:
                   self.blacklist.pop(self.blacklist.index((moveTo[0]+1,moveTo[1]+1)))
            elif moveFrom[0]-moveTo[0] < -1: #derecha
                if (moveTo[0]-1,moveTo[1]+1) in self.blacklist:
                   self.blacklist.pop(self.blacklist.index((moveTo[0]-1,moveTo[1]+1)))
            self.gameWon = -1
        else:
            raise Exception
    
    def moveBlack(self, moveFrom, moveTo, winLoss):
        self.moveSilentBlack(moveFrom, MoveTo, winLoss)
        self.printBoard()
        
    def moveWhite(self, moveFrom, moveTo, winLoss):
        self.moveSilentWhite(moveFrom, moveTo, winLoss)
        self.printBoard()

    def printBoard(self):
        print unicode(self)
        
    def __unicode__(self):
        self.updateBoard()
        lines = []
        lines.append('    ' + '   '.join(map(str, range(self.width))))
        lines.append(u'  ╭' + (u'───┬' * (self.width-1)) + u'───╮')
        for num, row in enumerate(self.boardState[:-1]):
            lines.append(chr(num+65) + u' │ ' + u' │ '.join(row) + u' │')
            lines.append(u'  ├' + (u'───┼' * (self.width-1)) + u'───┤')
        lines.append(chr(self.height+64) + u' │ ' + u' │ '.join(self.boardState[-1]) + u' │')
        lines.append(u'  ╰' + (u'───┴' * (self.width-1)) + u'───╯')
        return '\n'.join(lines)